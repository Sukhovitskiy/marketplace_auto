package com.example.auto.repository;

import com.example.auto.entity.Auto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AutoRepository extends JpaRepository<Auto, Integer> {
}