package com.example.auto.repository;

import com.example.auto.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface PhotoRepository extends JpaRepository<Photo, Integer> {
}