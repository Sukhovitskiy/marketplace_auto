package com.example.auto.controller;

import com.example.auto.entity.Auto;
import com.example.auto.service.interf.AutoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@Controller
@RequestMapping("/auto")
@AllArgsConstructor
@Tag(name = "Auto controller")
public class AutoController {
    private final AutoService autoService;

    @GetMapping("/all")
    public String showAll(Model model){
        model.addAttribute("autos", autoService.getAllAuto());
        return "index";
    }

    @GetMapping("/add")
    public String showAddForm(Model model){
        model.addAttribute("auto",new Auto());
        return "add_auto";
    }

    @PostMapping("/addAuto")
    public String addAutoSubmitButton(Auto auto){
        System.out.println("auto = " + auto);
        autoService.save(auto);
        return "index";
    }
}