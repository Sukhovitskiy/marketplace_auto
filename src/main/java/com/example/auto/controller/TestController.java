package com.example.auto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {
  @GetMapping("/test")
  public String showTest(Model model){
      model.addAttribute("text", "hello world");
      return "test";
  }
}
