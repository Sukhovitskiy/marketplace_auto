package com.example.auto.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;


import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "auto")
public class Auto {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "mark")
    private String mark;

    @Column(name = "model")
    private String model;

    @Column(name = "generation")
    private String generation;

    @Column(name = "engine_type")
    private String engineType;

    @Column(name = "transmission_type")
    private String transmissionType;

    @Column(name = "color")
    private String color;

    @Column(name = "fuel_type")
    private String fuelType;

    @Column(name = "mileage")
    private String mileage;

    @Column(name = "condition")
    private String condition;

    @Column(name = "owners")
    private String owners;

    @Column(name = "price")
    private String price;

    @Column(name = "address")
    private String address;

    @Column(name = "release_year")
    private String releaseYear;

    @OneToMany(mappedBy = "auto", fetch = FetchType.EAGER )
    @JsonIgnoreProperties("auto")
    private List<Photo> photos;
}