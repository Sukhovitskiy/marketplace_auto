package com.example.auto.service.impl;

import com.example.auto.entity.Auto;
import com.example.auto.repository.AutoRepository;
import com.example.auto.service.interf.AutoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class AutoServiceImpl implements AutoService {

    private final AutoRepository autoRepository;

    @Override
    @Transactional
    public List<Auto> getAllAuto() {
        return autoRepository.findAll();
    }
}