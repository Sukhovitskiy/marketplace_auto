package com.example.auto.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.example.auto.entity.Auto;
import com.example.auto.entity.Photo;
import com.example.auto.repository.AutoRepository;
import com.example.auto.repository.PhotoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class PhotoService {

    private final PhotoRepository fileRepository;
    private final AutoRepository autoRepository;


    public void save(MultipartFile file, Integer autoId) throws IOException {
        Photo fileEntity = new Photo();
        fileEntity.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileEntity.setContentType(file.getContentType());
        fileEntity.setData(file.getBytes());
        fileEntity.setSize(file.getSize());
        Auto auto = autoRepository.getById(autoId);
        fileEntity.setAuto(auto);

        fileRepository.save(fileEntity);
    }

    public Optional<Photo> getFile(Integer id) {
        return fileRepository.findById(id);
    }

    public List<Photo> getAllFiles() {
        return fileRepository.findAll();
    }
}