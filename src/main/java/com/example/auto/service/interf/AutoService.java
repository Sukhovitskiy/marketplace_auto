package com.example.auto.service.interf;

import com.example.auto.entity.Auto;

import java.util.List;

public interface AutoService {
    List<Auto> getAllAuto();
}