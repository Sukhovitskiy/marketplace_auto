create table if not exists auto (
    id integer not null,
    mark varchar(25),
    model varchar(25),
    generation varchar(25),
    engine_type varchar(25),
    transmission_type varchar(25),
    color varchar(25),
    fuel_type varchar(25),
    mileage varchar(25),
    condition varchar(25),
    owners varchar(25),
    price varchar(25),
    address varchar(25),
    release_year varchar(25)
)